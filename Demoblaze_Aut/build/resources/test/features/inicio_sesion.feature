Feature: Yo como cliente del almacén demoblaze
  Necesito iniciar sesion en la pagina de manera exitosa

  Background:
    Given el usuario ingresa a la pagina

  @CrearCuenta
    Scenario: Creacion de cuenta exitosa
    When el usuario crea una cuenta para iniciar sesion

  @LogInExitoso
  Scenario: Resultado de busqueda exitoso
    When el usuario inicia sesion con exito

  @LogInFallido
  Scenario: Log in de usuario fallido
    When el usuario inicia sesion con una cuenta inexistente