package co.com.test.demoblaze.questions;

import co.com.test.demoblaze.userinterfaces.LaptopDell;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class ValidarDescripcion implements Question {
    @Override
    public Object answeredBy(Actor actor) {
        return LaptopDell.LAPTOP_DESCRIPCION.resolveFor(actor).getText().equals("Product description");
    }
    public static Question <Boolean> ValidarDescripcion() {
        return new ValidarDescripcion();
    }
}

