package co.com.test.demoblaze.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class PaginaInicio {
    public static final Target LAPTOP = Target.the("Click en laptop").
            locatedBy("//a[contains(text(),'Laptops')]");
    public static final Target SIGN_UP_BOTON = Target.the("Click en sign up boton").
            located(By.id("signin2"));
    public static final Target CAMPO_USERNAME = Target.the("campo de username").
            located(By.id("sign-username"));
    public static final Target CAMPO_PASSWORD = Target.the("campo de password").
            located(By.id("sign-password"));
    public static final Target CREAR_CUENTA = Target.the("Click en crear cuenta").
            locatedBy("//button[contains(text(),'Sign up')]");
    public static final Target LOG_IN_BOTON = Target.the("Click en log in boton").
            located(By.id("login2"));
    public static final Target USUARIO_LOG_IN = Target.the("campo de username").
            located(By.id("loginusername"));
    public static final Target PASSWORD_LOG_IN = Target.the("campo de username").
            located(By.id("loginpassword"));
    public static final Target INICIAR_SESION = Target.the("Click en crear cuenta").
            locatedBy("//button[contains(text(),'Log in')]");


}
