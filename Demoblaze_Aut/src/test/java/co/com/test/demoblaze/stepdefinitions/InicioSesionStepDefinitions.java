package co.com.test.demoblaze.stepdefinitions;

import co.com.test.demoblaze.tasks.CrearCuenta;
import co.com.test.demoblaze.tasks.InicioSesion;
import co.com.test.demoblaze.tasks.InicioSesionFallido;
import co.com.test.demoblaze.tasks.ResultadosBusqueda;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class InicioSesionStepDefinitions {

    @Managed(driver = "chrome")
    WebDriver hisdriver;
    @Before
    public void setThestago() {
        OnStage.setTheStage(new OnlineCast());
        OnStage.theActorCalled("Andres");
    }

    @When("^el usuario crea una cuenta para iniciar sesion$")
    public void elUsuarioCreaUnaCuentaParaIniciarSesion() {
        theActorInTheSpotlight().attemptsTo(CrearCuenta.crearCuenta());
    }

    @When("^el usuario inicia sesion con exito$")
    public void elUsuarioIniciaSesionConExito() {
        theActorInTheSpotlight().attemptsTo(InicioSesion.inicioSesion());
    }

    @When("^el usuario inicia sesion con una cuenta inexistente$")
    public void elUsuarioIniciaSesionConUnaCuentaInexistente() {
        theActorInTheSpotlight().attemptsTo(InicioSesionFallido.inicioSesionFallido());

    }
}
