package co.com.test.demoblaze.stepdefinitions;

import co.com.test.demoblaze.questions.ValidarDescripcion;
import co.com.test.demoblaze.questions.ValidarNombre;
import co.com.test.demoblaze.questions.ValidarPrecio;
import co.com.test.demoblaze.tasks.ResultadosBusqueda;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
public class ResultadoBusquedaStepDefinitions {

    @Managed(driver = "chrome")
    WebDriver hisdriver;
    @Before
    public void setThestago() {
        OnStage.setTheStage(new OnlineCast());
        OnStage.theActorCalled("Andres");
    }

    @Given("^el usuario ingresa a la pagina$")
    public void elUsuarioIngresaALaPagina() {
        theActorInTheSpotlight().wasAbleTo(Open.url("https://www.demoblaze.com"));
    }

    @When("^el usuario consulta un laptop$")
    public void elUsuarioConsultaUnLaptop() {
        theActorInTheSpotlight().attemptsTo(ResultadosBusqueda.busqueda());
    }

    @Then("^el usuario ve el nombre del producto$")
    public void elUsuarioVeElNombreDelProducto() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(ValidarNombre.ValidarNombre()));
    }

    @Then("^el usuario ve el precio del producto$")
    public void elUsuarioVeElPrecioDelProducto() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(ValidarPrecio.ValidarPrecio()));
    }

    @Then("^el usuario ve la descripcion del producto$")
    public void elUsuarioVeLaDescripcionDelProducto() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(ValidarDescripcion.ValidarDescripcion()));
    }

}
